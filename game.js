
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.tilemap('map', 'tilemaps/MAP-1.json', null, Phaser.Tilemap.TILED_JSON);

    game.load.image('Overworld', 'tilemaps/gfx/Overworld.png');

    game.load.spritesheet('phaser', 'hero.png', 64, 64);
  

}

var map;
var layer;
var layer3;
var sprite;
var cursors;
var upKey;
var downKey;
var leftKey;
var rightKey;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    map = game.add.tilemap('map');

    map.addTilesetImage('Overworld');


    map.setCollisionBetween(1, 12);

    //  This will set Tile ID 26 (the coin) to call the hitCoin function when collided with
    map.setTileIndexCallback(26, hitCoin, this);

    //  This will set the map location 2, 0 to call the function
    map.setTileLocationCallback(2, 0, 1, 1, hitCoin, this);

    layer = map.createLayer('Tile Layer 1');
    layer.resizeWorld();
    layer.debugSettings.forceFullRedraw = true;

    layer3 = map.createLayer('Tile Layer 3');


    sprite = game.add.sprite(260, 100, 'phaser');
    sprite.anchor.set(0.5);
    game.physics.enable(sprite);

    sprite.body.setSize(0,0,0,0);

    //  We'll set a lower max angular velocity here to keep it from going totally nuts
    sprite.body.maxAngular = 500;

    //  Apply a drag otherwise the sprite will just spin and never slow down
    sprite.body.angularDrag = 50;

    game.camera.follow(sprite);

    cursors = game.input.keyboard.createCursorKeys();
    upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    
    sprite.animations.add('walkUp', [193, 194, 195, 196, 197, 198, 199], 8, true);
    sprite.animations.add('walkDown', [244, 244, 245, 246, 247, 248], 8, true);
    sprite.animations.add('walkRight',[264, 265, 266, 267, 268, 269], 8, true);
    sprite.animations.add('walkLeft', [219, 220, 221, 222, 223, 224], 8, true);
    
    game.input.keyboard.onUpCallback = function (e) {
   
        sprite.animations.stop();
};

}

function hitCoin(sprite, tile) {

    tile.alpha = 0.2;

    layer.dirty = true;

    return false;

}

function update() {


    game.physics.arcade.collide(sprite, layer);

    sprite.body.velocity.x = 0;
    sprite.body.velocity.y = 0;
    sprite.body.angularVelocity = 0;
    

    if (upKey.isDown)
    {
        sprite.y--;
        sprite.animations.play('walkUp');
        
    }
    else if (downKey.isDown)
    {
        sprite.y++;
        sprite.animations.play('walkDown');

    } 

    if (leftKey.isDown)
    {
        sprite.x--;
        sprite.animations.play('walkLeft');
    
    }
    else if (rightKey.isDown)
    {
        sprite.x++;
        sprite.animations.play('walkRight'); 
       
    } 
 

}

function render() {

    game.debug.body(sprite);

}
 
